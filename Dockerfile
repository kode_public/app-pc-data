# R enviroment image for shiny server

FROM rocker/shiny:3.6.3
MAINTAINER Ilaria Ceppa <i.ceppa@kode-solutions.net>

RUN apt-get update -qq && apt-get install -y \
  git-core \
  libssl-dev \
  libcurl4-gnutls-dev \
  zlib1g-dev \
  libssh2-1-dev		\
  libxml2-dev \
  libsasl2-dev \
  libpq-dev \
  libudunits2-dev \
  libjpeg-dev \
  libv8-dev \   
  libgdal-dev \
  libjq-dev \
  libprotobuf-dev \
  protobuf-compiler \
  cron

RUN R -e 'install.packages(c("devtools", "tidyverse", "shiny", "plotly", "jsonlite", "DT", "httr"))'
RUN R -e 'install.packages(c("shinyWidgets"))'
RUN R -e 'install.packages(c("shinyjs", "v8"))'
RUN R -e 'install.packages(c("RPostgreSQL", "rjson"))'
RUN R -e 'devtools::install_version("rgeos", version = "0.3-28")'
RUN R -e 'install.packages(c("sf"))'
RUN R -e 'install.packages(c("sp"))'
RUN R -e 'install.packages(c("rgdal"))'
RUN R -e 'install.packages(c("geojsonsf"))'
RUN R -e 'install.packages(c("geojsonio"))'
RUN R -e 'install.packages(c("leaflet"))'

RUN R -e 'devtools::install_version("bs4Dash", version = "0.4.0")'
RUN R -e 'install.packages(c("shinycssloaders", "ggsci"))'

# use custom shiny-server.conf file
#COPY . /srv/shiny-server/
#WORKDIR /usr/local/shiny_files/
#RUN /bin/bash -c 'cp shiny-server.conf /etc/shiny-server/shiny-server.conf'

RUN echo "30 18 * * * touch /srv/shiny-server/covid-dashboard/restart.txt" >> covid-cron
RUN crontab covid-cron


EXPOSE 3838

CMD ["/usr/bin/shiny-server.sh"]

